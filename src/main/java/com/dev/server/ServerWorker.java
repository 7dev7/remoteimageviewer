package com.dev.server;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ServerWorker {
    private static final Logger LOGGER = Logger.getLogger(ServerWorker.class);
    private FileHandler handler;

    public ServerWorker(String serverPicsDirectory) {
        handler = new FileHandler(serverPicsDirectory);
    }

    public void doWork(Socket socket) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

            List<String> filesInDirectory = handler.getFilesInDirectory();
            out.writeObject(filesInDirectory);
            out.flush();

            int userChoice = in.readInt();

            sendFile(out, handler.getFilePathByIndex(userChoice));
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                LOGGER.error("Couldn't close socket", e);
            }
        }
    }

    private void sendFile(ObjectOutputStream out, Path pathToFile) throws IOException {
        byte[] fileContent = Files.readAllBytes(pathToFile);
        out.writeUTF(pathToFile.getFileName().toString());
        out.writeObject(fileContent);
    }
}
