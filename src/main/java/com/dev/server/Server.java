package com.dev.server;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final Logger LOGGER = Logger.getLogger(Server.class);
    private ExecutorService executorService;
    private Properties properties;
    private ServerSocket serverSocket;
    private boolean serverIsRunning;

    public Server(String pathToConfig) {
        properties = PropertiesLoader.loadProperties(pathToConfig);
        executorService = Executors.newFixedThreadPool(Integer.parseInt(properties.getProperty(Constants.THREAD_POOL_PROP)));
    }

    public Server() {
        this(Constants.DEFAULT_CONFIG_FILE);
    }

    public void start() {
        serverIsRunning = true;
        final int port = Integer.parseInt(properties.getProperty(Constants.SERVER_PORT_PROP));
        try {
            serverSocket = new ServerSocket(port);
            while (serverIsRunning) {
                System.out.println("Waiting for a new connection...");
                Socket socket = serverSocket.accept();
                ServerWorker worker = new ServerWorker(properties.getProperty(Constants.PATH_TO_PICS_PROP));
                executorService.execute(() -> worker.doWork(socket));
            }
        } catch (IOException e) {
            LOGGER.error("Couldn't accept socket", e);
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    LOGGER.error("Couldn't close serverSocket", e);
                }
            }
        }
    }

    public void shutdown() {
        serverIsRunning = false;
        try {
            serverSocket.close();
            executorService.shutdown();
        } catch (IOException e) {
            LOGGER.error("Couldn't close serverSocket", e);
        }
    }
}
