package com.dev.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        Server server = new Server();

        Thread thread = new Thread(server::start);
        thread.setDaemon(true);
        thread.start();

        listenUserInput(server);
    }

    private static void listenUserInput(Server server) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                if ("stop".equals(line.trim().toLowerCase())) {
                    server.shutdown();
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("Exception during user's input:(");
        }
    }
}
