package com.dev.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class FileHandler {
    private String path;
    private List<String> picsPath = new ArrayList<>();

    public FileHandler(String path) {
        this.path = path;
    }

    public List<String> getFilesInDirectory() {
        List<String> files = new ArrayList<>();
        try {
            Files.walk(Paths.get(path)).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    picsPath.add(filePath.toString());
                    files.add(filePath.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;
    }

    public Path getFilePathByIndex(int index) {
        return Paths.get(picsPath.get(index));
    }
}
