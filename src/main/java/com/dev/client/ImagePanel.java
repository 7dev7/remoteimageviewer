package com.dev.client;

import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImagePanel extends JPanel {
    private static final Logger LOGGER = Logger.getLogger(ImagePanel.class);
    private BufferedImage image;

    public ImagePanel(File fileImage) {
        try {
            image = ImageIO.read(fileImage);
        } catch (IOException e) {
            LOGGER.error("Couldn't read image", e);
        }
        setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }
}
