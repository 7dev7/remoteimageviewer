package com.dev.client;

import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.*;
import java.net.Inet4Address;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Client {
    private static final int PORT = 14777;
    private static final Logger LOGGER = Logger.getLogger(Client.class);

    public void connect() {
        try (Socket socket = new Socket(Inet4Address.getLocalHost(), PORT)) {
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

            List<String> files = (List<String>) in.readObject();
            printFiles(files);
            int userChoice = getUserInputWithValidation(files.size());

            out.writeInt(userChoice);
            out.flush();

            String fileName = in.readUTF();
            byte[] fileContent = (byte[]) in.readObject();
            showPic(fileName, fileContent);
        } catch (IOException e) {
            LOGGER.error(e);
            System.out.println("Couldn't connect to server:(");
        } catch (ClassNotFoundException e) {
            LOGGER.error("Unexpected value from server", e);
        }
    }

    private int getUserInputWithValidation(int maxValue) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.println("Input number of pic: ");
                String input = reader.readLine();
                Integer num = Integer.parseInt(input);
                if (num < 0 || num >= maxValue) {
                    System.out.println("Incorrect value");
                    continue;
                }
                return num;
            } catch (IOException e) {
                LOGGER.error("Couldn't read value from server", e);
                return -1;
            } catch (NumberFormatException e) {
                LOGGER.error(e);
                System.out.println("Isn't a number:(");
            }
        }
    }

    private void printFiles(List<String> files) {
        for (int i = 0; i < files.size(); i++) {
            System.out.println(i + ": " + files.get(i));
        }
    }

    private void showPic(String fileName, byte[] fileContent) throws IOException {
        Path pathToFile = Files.write(File.createTempFile(fileName, "").toPath(), fileContent);

        JFrame frame = new JFrame();
        ImagePanel panel = new ImagePanel(new File(pathToFile.toString()));

        panel.setVisible(true);
        frame.getContentPane().add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
